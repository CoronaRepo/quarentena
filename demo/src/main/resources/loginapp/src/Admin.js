import React from 'react';
import ReactDOM from 'react-dom';
import { RegisterService } from './services/RegisterService'

import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/components/column/Column';
import {Panel} from 'primereact/panel';
import {Menubar} from 'primereact/menubar';
import {Dialog} from 'primereact/dialog';
import {InputText} from 'primereact/inputtext';
import {Button} from 'primereact/button';
import {Growl} from 'primereact/growl';
import { Link } from 'react-router-dom';
import  logoADP  from './images/logoADP.png';
import Register from './Register';

import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';

class Admin extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            user:{
                id:null,
                username:null,
                email:null,
                password:null,
                roles:[]
                },
        };
        this.items = [
            {
                label:'User Board',
            }
        ]
        this.registerservice = new RegisterService();
    }

componentDidMount(){
    this.registerservice.getAll().then(data => this.setState({user: data}));

}


    render(){
          return (
            <div className="p-grid p-dir-col">
                <div className="p-col-2 p-col-align-end" >
                    <img  style={{width:'50%',marginTop:'20px'}} src={logoADP} alt="logo"/>
                </div>
                <div style={{width:'80%', marginTop:'4%', margin:'0 auto'}}>
                    <Menubar  model={this.items} style={{border:'0px', fontSize: '100%', paddingLeft: '50%', background: '#d3d3d3'}}>
                        <div style={{marginTop:'1%'}}>
                            <InputText placeholder="Search" type="text"/>
                            <Button label="Logout" icon="pi pi-power-off" className="p-button-secondary" style={{marginLeft:4}}/>
                        </div>
                    </Menubar>
                </div>
                <div style={{width:'80%', margin:'0 auto'}}>
                    <h1> Admin Board </h1>
                    <div style={{width:'30%', marginLeft:'78%'}}>
                        <Link to="/Register" >
                            <Button label="Add User" className="p-button-success" />
                        </Link>
                        <Button label="Update" className="p-button-primary" style={{ marginLeft:'2%'}}/>
                        <Button label="Delete" className="p-button-danger" style={{ marginLeft:'2%'}}/>
                    </div>
                <br/>
                    <Panel header="Users" style={{textAlign:'center', marginTop:'20px'}}>
                     <DataTable value={this.state.user}>
                        <Column field="id" header="id"></Column>
                        <Column field="username" header="Username"></Column>
                        <Column field="email" header="E-mail"></Column>
                        <Column field="password" header="Password"></Column>
                        <Column header="Role"></Column>
                     </DataTable>
                <br/>
                    </Panel>
                </div>
            </div>
          );
    }

}
export default Admin;