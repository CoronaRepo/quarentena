import React from 'react';
import './App.css';

import { CSSTransitionGroup } from 'react-transition-group'
import  logoADP  from './images/logoADP.png';
import  userICON  from './images/userICON.png';

import {Button} from 'primereact/button';
import {InputText} from 'primereact/inputtext';
import {Panel} from 'primereact/panel';
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';

class Login extends React.Component {
    render() {

    return  <div class="p-grid p-dir-col">
                <div class="p-col-2 p-col-align-end" >
                    <img  style={{width:'50%',marginTop:'20px'}} src={logoADP} alt="logo"/>
                </div>
                <div style={{width:'25%', margin:'0 auto', marginTop:'50px'}}>
                    <div class="p-col-4 p-offset-4"  >
                        <img style={{width:'100%', marginBottom:'-15px', color: "white"}} src={userICON}/>
                    </div>
                    <Panel header="Sign In" style={{textAlign:'center'}}>
                    <br/>
                   <div className="p-grid p-fluid" >
                       <div className="p-col-12 p-md-5"  >
                           <div className="p-inputgroup" style={{width:'250%'}}>
                               <span className="p-inputgroup-addon">
                                   <i className="pi pi-user"></i>
                               </span>
                               <InputText placeholder="Username" />
                           </div>
                       </div>
                   </div>
                    <div className="p-grid p-fluid"  >
                       <div className="p-col-12 p-md-5">
                           <div className="p-inputgroup" style={{width:'250%'}}>
                               <span className="p-inputgroup-addon">
                                   <i className="pi pi-lock"></i>
                               </span>
                               <InputText placeholder="Password" />
                           </div>
                       </div>
                    </div>
                    <br/>
                    <Button label="Login" className="p-button-rounded" style={{width:'40%'}} />
                    <div className="p-grid p-fluid" style={{marginLeft:'50%',  marginTop:'5%'}}>
                            <div className="p-col-12 p-md-12">
                                <h5 > Forgot Password? </h5>
                            </div>
                    </div>
                    </Panel>
                </div>
            </div>
    }
}


export default Login;
